#include "algorithms.h"
#include "basic_memory_classes.h"
#include <cstdlib>
#include <iostream>

unsigned random(unsigned n) {
    return (unsigned)((double)rand() / (double)RAND_MAX * n);
}

ReplacementStrategy::ReplacementStrategy(unsigned block_count) : block_count(block_count) {}

ReplacementStrategy::~ReplacementStrategy() {}

CacheIterator ReplacementStrategy::find(CacheIterator begin, CacheIterator end) {
    return begin;
}

void ReplacementStrategy::change_structure_new(CacheIterator iter) {}

void ReplacementStrategy::change_structure_old(CacheIterator iter) {}

RandomStrategy::RandomStrategy (unsigned block_count, unsigned seed) : ReplacementStrategy(block_count) {
    srand(seed);
}

CacheIterator RandomStrategy::find(CacheIterator begin, CacheIterator end) { 
    unsigned count = 0;
    for (auto it = begin; it != end; ++it) {
        if ((*it).init) {
            ++count;
        }
        else {
            return it;
        }
    }
    unsigned index = random(count);
    for (unsigned i = 0; i < index; ++i) {
        ++begin;
    }
    return begin;
}

LRU_Strategy::LRU_Strategy (unsigned block_count) : ReplacementStrategy(block_count), queue(block_count, 0) {}

CacheIterator LRU_Strategy::find(CacheIterator begin, CacheIterator end) { 
    unsigned count = 0;
    CacheIterator cur = begin;
    for (auto it = begin; it != end; ++it) {
        if ((*it).init) {
            if (queue[(*cur).id] < queue[(*it).id]) {
                cur = it;
            }
            ++count;
        }
        else {
            return it;
        }
    }
    return cur;
}

void LRU_Strategy::change_structure(CacheIterator iter) {
    auto begin = iter.to_begin();
    auto end = iter.to_end();
    unsigned cur_prior = queue[(*iter).id];
    for (auto it = begin; it != end; ++it) {
        if (queue[(*it).id] <= cur_prior) {
            ++queue[(*it).id];
        }
    }
    queue[(*iter).id] = 0;
}

void LRU_Strategy::change_structure_new(CacheIterator iter) {
    change_structure(iter);
}

void LRU_Strategy::change_structure_old(CacheIterator iter) {
    change_structure(iter);
}

LFU_Strategy::LFU_Strategy(unsigned block_count, unsigned max_value, unsigned init_value,
                           unsigned become_old, unsigned make_young, unsigned seed) :
                           ReplacementStrategy(block_count), max_value(max_value), init_value(init_value),
                           become_old(become_old), make_young(make_young), age(block_count, 0),
                           cur_old(block_count, 0) {
    srand(seed);
}

CacheIterator LFU_Strategy::find(CacheIterator begin, CacheIterator end) { 
    std::vector<CacheIterator> candidates;
    unsigned min_value = max_value;
    for (auto it = begin; it != end; ++it) {
        if (age[(*it).id] < min_value) {
            candidates.clear();
            min_value = age[(*it).id];
        }
        if (age[(*it).id] == min_value) {
            candidates.push_back(it);
        }
    }
    unsigned index = random(candidates.size());
    return candidates[index];
}

void LFU_Strategy::change_structure_new(CacheIterator iter) {
    age[(*iter).id] = init_value;
}

void LFU_Strategy::change_structure_old(CacheIterator iter) {
    if (age[(*iter).id] < max_value) {
        ++age[(*iter).id];
    }
    auto begin = iter.to_begin();
    auto end = iter.to_end();
    for (auto it = begin; it != end; ++it) {
        ++cur_old[(*it).id];
    }
    if (cur_old[(*iter).id] == become_old) {
        for (auto it = begin; it != end; ++it) {
            cur_old[(*it).id] = 0;
            age[(*it).id] /= make_young;
        }
    }
}



WriteStrategy::WriteStrategy(unsigned block_count) : block_count(block_count), hit_write_direct(true) {}

WriteStrategy::~WriteStrategy() {}

bool WriteStrategy::dirty(addr_t id) {
    return false;
}

void WriteStrategy::set(addr_t id, bool flag) {}

WriteBackStrategy::WriteBackStrategy(unsigned block_count) : WriteStrategy(block_count),
                                                             dirty_blocks(block_count, false) {
    hit_write_direct = false;
}

bool WriteBackStrategy::dirty(addr_t id) {
    return dirty_blocks[id];
}

void WriteBackStrategy::set(addr_t id, bool flag) {
    dirty_blocks[id] = flag;
}


