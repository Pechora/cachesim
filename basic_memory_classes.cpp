#include "basic_memory_classes.h"
#include <iostream>

MemoryCell::MemoryCell() : value(0), init(false) {}

MemoryCell::MemoryCell(mem_t value) : value(value), init(true) {}

mem_t MemoryCell::get() {
    return value;
}

void CacheBlock::set_id(unsigned new_id) {
    id = new_id;
}

CacheBlock::CacheBlock(unsigned block_size) : id(0), tag(0), init(false), cells(block_size, MemoryCell()) {}

void CacheBlock::load_from_RAM(MemoryCell *mem_cell, addr_t addr) {
    init = true;
    tag = addr / cells.size();
    for (unsigned i = 0; i < cells.size(); ++i) {
        cells[i] = mem_cell[i - addr % cells.size()];
    }
}

void CacheBlock::write_to_RAM(std::vector<MemoryCell> &memory) {
    addr_t addr = tag * cells.size();
    for (unsigned i = 0; i < cells.size(); ++i) {
        memory[addr + i] = cells[i];
    }
}

void CacheBlock::load_direct(addr_t addr, unsigned size, mem_t* value) {
    addr %= cells.size();
    for (unsigned i = 0; i < size; ++i) {
        cells[addr + i] = MemoryCell(value[i]);
    }
}

CacheIterator::CacheIterator(CacheBlock *begin, addr_t index, unsigned block_count, unsigned associativity) :
                             begin(begin), index(index), block_count(block_count), associativity(associativity) {}

CacheIterator& CacheIterator::operator++() {
    index += associativity;
    if (index > block_count) {
        index = block_count;
    }
    return *this;
}

CacheBlock& CacheIterator::operator *() {
    return *(begin + index);
}

bool CacheIterator::operator ==(CacheIterator it) {
    return (begin == it.begin && index == it.index);
}

bool CacheIterator::operator !=(CacheIterator it) {
    return !CacheIterator::operator ==(it);
}

CacheIterator CacheIterator::to_begin() {
    CacheIterator tmp(*this);
    tmp.index = 0;
    return tmp;
} 

CacheIterator CacheIterator::to_end() {
    CacheIterator tmp(*this);
    tmp.index = block_count;
    return tmp;
}








