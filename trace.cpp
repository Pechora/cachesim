#include "trace.h"
#include <string>
#include <istream>
#include <iostream>

Trace::Trace() : line(0) {}

Trace::~Trace() {}

std::string Trace::cut_comment(std::string str) {
    size_t i = 0;
    while (i < str.size() && str[i] != '#') {
        ++i;
    }
    return str.substr(0, i); 
}

bool Trace::fill_trace(std::istream &input) {
    std::string word;    
    input >> word;
    try {
        word = cut_comment(word);
        if (input && word == "") {
            std::getline(input, word);
            ++line;
            return Trace::fill_trace(input);
        }
        if (word[0] == 'R') {
            com = READ;
        }
        else {
            com = WRITE;
        }
        input >> word;
        word = cut_comment(word);
        addr = std::stoul(word, NULL, 16);/////////////////
        size = 1;
        value = 0;
    }
    catch(...) {
        if (word == "") {
            return false;
        }
        else {
            std::cout << "\nIncorrect trace\n";
        }
        std::cout << "Line " << line << "\n";
        exit(1);
    }
    return true;
}

bool Trace_full::fill_trace(std::istream &input) {
    bool fl = Trace::fill_trace(input);
    if (!fl) {
        return false;
    }
    std::string word;
    input >> word;
    try {
        word = cut_comment(word);
        size = std::stoul(word, NULL, 10);
        input >> word;
        word = cut_comment(word);
        value = std::stoll(word, NULL, 10);
    }
    catch(...) {
        if (word == "") {
            return false;
        }
        else {
            std::cout << "\nIncorrect trace\n";
        }
        std::cout << "Line " << line << "\n";
        exit(1);
    }
    return true;
}

bool Trace::get_trace(std::istream &input) {
    ++line;
    bool fl = fill_trace(input);
    std::string str;
    std::getline(input, str);
    return fl;
}

