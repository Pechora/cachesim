#include <map>
#include <string>
#include <iostream>
#include "trace.h"
#include "memory.h"
#include "parse_config.h"

union Value {
    unsigned char bytes[8];
    long long val;
};

int main(int argc, char *argv[]) {
    Config config;
    if (argc > 1) {
        config = Config(argv[1]);
    }
    else {
        config = Config("config");
    }
    Trace *trace;
    if (config.find("trace", "trace") != config.end()) {
        auto value = (*config.find("trace", "trace")).value;
        if (value == "simple") {
            trace = new Trace();
        }
        else {
            trace = new Trace_full();
        }
    }
    else {
        trace = new Trace();
    }
    Memory memory(config);
    Value value;
    while(trace->get_trace(std::cin)) {
        value.val = trace->value;
        if (trace->com == Trace::READ) {
            memory.read(trace->addr, trace->size, value.bytes);
        }
        else {
            memory.write(trace->addr, trace->size, value.bytes);
        }
    }
    memory.print();
    delete trace;
}

