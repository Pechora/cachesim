#include "parse_config.h"
#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>

std::string Config::cut_spaces(std::string str) {
    size_t i = 0;
    while (i < str.size() && str[i] == ' ') {
        ++i;
    }
    if (i == str.size()) {
        return "";
    }
    size_t j = str.size() - 1;
    while (j >= i && str[j] == ' ') {
        --j;
    }
    return str.substr(i, j - i + 1); 
}

Config::Config() {}

Config::Config(std::string config_name) {
    std::ifstream input(config_name);
    std::string str;
    std::string section = "";
    while (std::getline(input, str)) {
        str = cut_spaces(str);
        if (str.size() == 0) {
            continue;
        }
        if (str.find("#") != std::string::npos) {
            int pos = str.find("#");
            if (pos <= 0) {
                continue;
            }
            if (str[pos - 1] != '\\') {
                for (size_t i = pos; i != str.size(); ++i) {
                    str[i] = ' ';
                }
            }
            else {
                str[pos - 1] = ' ';
                for (size_t i = pos; i < str.size(); ++i) {
                    std::swap(str[i], str[i - 1]);
                } 
            }
            str = cut_spaces(str);
        }
        if (str.find("=") != std::string::npos) {
            int pos = str.find("=");
            std::string key = cut_spaces(str.substr(0, pos));
            std::string value = cut_spaces(str.substr(pos + 1, str.size() - pos - 1));
            config[section][key] = value;
        }
        else {
            if (*str.begin() == '[' && *(str.end() - 1) == ']') {
                std::string pos_sec = cut_spaces(str.substr(1, str.size() - 2));
                if (pos_sec != "") {
                    section = pos_sec;
                }
            }
            else {
                continue;
            }
        }
    }
    input.close();
}

Config::Item::Item(std::string section, std::string key, std::string value) : section(section), key(key), value(value) {}

long long Config::Item::get_int() {
    long long res;
    try {
        res = std::stoll(value);
    }
    catch(...) {
        std::cout << "Incorrect config\nSection: " << section << "    key: " << key << "\n";
        exit(1);
    }
    return res;
}

Config::Config_iterator::Config_iterator (std::map<std::string, std::map<std::string, std::string> >::const_iterator section_it,
                                          std::map<std::string, std::map<std::string, std::string> >::const_iterator section_end) :
                                          section_it(section_it), key_value_it(), section_end(section_end) {}

Config::Config_iterator::Config_iterator (std::map<std::string, std::map<std::string, std::string> >::const_iterator section_it,
                                          std::map<std::string, std::string>::const_iterator key_value_it,
                                          std::map<std::string, std::map<std::string, std::string> >::const_iterator section_end) :
                                          section_it(section_it), key_value_it(key_value_it), section_end(section_end) {}

Config::Config_iterator& Config::Config_iterator::operator ++() {
    ++key_value_it;
    while (section_it != section_end && section_it->second.end() == key_value_it) {
        ++section_it;
        key_value_it = section_it->second.begin();
    }
    return *this;
}

Config::Config_iterator Config::Config_iterator::operator ++(int) {
    Config_iterator it(this->section_it, this->key_value_it, this->section_end);
    this->operator++();
    return it;
}

Config::Item Config::Config_iterator::operator * () {
    if (section_it == section_end) {
        std::cout << "\nConfig or key config parametr does not exist\n";
        exit(1);
    }
    return Item(section_it->first, key_value_it->first, key_value_it->second);
}

bool Config::Config_iterator::operator == (Config_iterator it) {
    if (section_it == section_end && it.section_it == it.section_end) {
        return true;
    }
    return (section_it == it.section_it && key_value_it == it.key_value_it);
}

bool Config::Config_iterator::operator != (Config_iterator it) {
    return !Config_iterator::operator==(it);
}

Config::Config_iterator Config::end() {
    return Config::Config_iterator(config.end(), config.end()); 
}

Config::Config_iterator Config::begin() {
    if (config.empty()) {
        return end();
    }
    return Config::Config_iterator(config.begin(), config.begin()->second.begin(), config.end()); 
}

Config::Config_iterator Config::find(std::string section, std::string key) {
    if (config[section].count(key)) {
        return Config_iterator(config.find(section), config[section].find(key), config.end());
    }
    else {
        return end();
    }
};


