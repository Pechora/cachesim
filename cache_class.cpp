#include "cache_class.h"
#include "algorithms.h"
#include "basic_memory_classes.h"
#include "parse_config.h"
#include <iostream>
#include <cstdio>

Cache::Cache(unsigned cache_read_time, unsigned cache_write_time,
             unsigned cache_size, unsigned block_size) : 
             associativity(0), cache_read_time(cache_read_time),
             cache_write_time(cache_write_time), cache_size(cache_size), block_size(block_size),
             cache_time(0), blocks(cache_size / block_size, CacheBlock(block_size)) {
    for (unsigned i = 0; i < cache_size / block_size; ++i) {
        blocks[i].set_id(i);
    }
    if (cache_size % block_size) {
        std::cout << "\nblock_size must divide cache_size\n";
        exit(1);
    }
}

void Cache::set_params(Config &config, unsigned cache_associativity,
                       Algorithm replacement_strategy, Algorithm write_strategy) {
    associativity = cache_associativity;
    if (associativity == 0) {
        std::cout << "\nwrong associativity\n";
        exit(1);
    }
    if (replacement_strategy == Algorithm::Random) {
        unsigned seed = 0;
        if (config.find("random", "seed") != config.end()) {
            seed = (*config.find("random", "seed")).get_int();
        }
        replacement = new RandomStrategy(cache_size / block_size, seed);
    }
    if (replacement_strategy == Algorithm::LRU) {
        replacement = new LRU_Strategy(cache_size / block_size);
    }
    if (replacement_strategy == Algorithm::LFU) {
        unsigned seed = 0;
        if (config.find("random", "seed") != config.end()) {
            seed = (*config.find("random", "seed")).get_int();
        }
        unsigned max_value = (*config.find("LFU", "max_value")).get_int();
        unsigned init_value = (*config.find("LFU", "init_value")).get_int();
        unsigned become_old = (*config.find("LFU", "become_old")).get_int();
        unsigned make_young = (*config.find("LFU", "shift")).get_int();
        replacement = new LFU_Strategy(cache_size / block_size,
                                       max_value, init_value, become_old, make_young, seed);
    }
    if (write_strategy == Algorithm::Write_through) {
        writes = new WriteStrategy(cache_size / block_size);
    }
    if (write_strategy == Algorithm::Write_back) {
        writes = new WriteBackStrategy(cache_size / block_size);
    }
}

Cache::~Cache() {
    delete replacement;
    delete writes;
}

CacheIterator Cache::find_replacement(addr_t addr) {
    return replacement->find(begin(addr), end(addr));
}

void Cache::change_structure_new(CacheIterator iter) {
    replacement->change_structure_new(iter);
}

void Cache::change_structure_old(CacheIterator iter) {
    replacement->change_structure_old(iter);
}

CacheIterator Cache::find_addr(addr_t addr) {
    addr_t tag = addr / block_size;
    for (auto it = begin(addr); it != end(addr); ++it) {
        if ((*it).init && (*it).tag == tag) {
            return it;
        }
    }
    return end(addr);
};

CacheIterator Cache::begin(addr_t addr) {
    unsigned block_count = cache_size / block_size;
    return CacheIterator(&blocks[(addr / block_size) % block_count % associativity], 0, block_count, associativity);
}

CacheIterator Cache::end(addr_t addr) {
    unsigned block_count = cache_size / block_size;
    return CacheIterator(&blocks[(addr / block_size) % block_count % associativity], block_count, block_count, associativity);
}

void Cache::read() {
    cache_time += cache_read_time;
}

void Cache::write() {
    cache_time += cache_write_time;
}

bool Cache::back_to_RAM(CacheIterator iter, std::vector<MemoryCell> &memory) {
    if (writes->dirty((*iter).id)) {
        (*iter).write_to_RAM(memory);
        writes->set((*iter).id, false);
        return true;
    }
    return false;
}

bool Cache::write_direct(CacheIterator iter, addr_t addr, unsigned size, mem_t* value) {
    (*iter).load_direct(addr, size, value);
    writes->set((*iter).id, true);
    return writes->dirty((*iter).id);
}

bool Cache::hit_write_direct() {
    return writes->hit_write_direct;
}

void Cache::print() {
    for (unsigned i = 0; i < cache_size / block_size; ++i) {
        std::cout << "\n";
        if (i % associativity == 0) {
            std::cout << "\n";
        }
        printf("%06d", blocks[i].id);
        std::cout << ": tag ";
        printf("%012X", blocks[i].tag);
        std::cout << "    ";
        if (blocks[i].init) {
            for (unsigned j = 0; j < block_size; ++j) {
                if (blocks[i].cells[j].init) {
                    printf("%02X", blocks[i].cells[j].get());
                }
                else {
                    std::cout << "..";
                }
                std::cout << " ";
            }
        }
        std::cout << "dirty: " << writes->dirty(i);
    }
    std::cout << "\n\n";
}

















