#include <vector>
#include <string>
#include <ostream>
#include "parse_config.h"
#include "cache_class.h"
#include "basic_memory_classes.h"

class RAM {
  private:
    unsigned RAM_read_time;
    unsigned RAM_write_time;
    unsigned RAM_width;

  public:
    std::vector<MemoryCell> memory;
    unsigned RAM_size;
    unsigned RAM_time;

    RAM() {}
    RAM(unsigned RAM_read_time, unsigned RAM_write_time, unsigned RAM_width, unsigned RAM_size);

    void read(unsigned size);

    void write(unsigned size);

    void write_direct(addr_t addr, unsigned size, mem_t *value);

    unsigned size();

    void print(unsigned block_size);
};

class Memory {
  private:
    unsigned reads;
    unsigned writes;
    unsigned read_misses;
    unsigned write_misses;
    unsigned back_writes;
    RAM ram;
    Cache cache;

    CacheIterator replace(addr_t addr);
    
  public:
    Memory(Config &config);

    void correct_addr(addr_t addr, unsigned size);
    
    void read(addr_t addr, unsigned size, mem_t *value);

    void write(addr_t addr, unsigned size, mem_t *value);

    void print_statistics();

    void print();
};


















