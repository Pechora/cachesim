#include <vector>
#include <utility>
#include <ostream>
#include <iostream>
#include <cstdio>
#include <string>
#include "memory.h"
#include "basic_memory_classes.h"
#include "cache_class.h"

RAM::RAM(unsigned RAM_read_time, unsigned RAM_write_time, unsigned RAM_width, unsigned RAM_size) :
         RAM_read_time(RAM_read_time), RAM_write_time(RAM_write_time), RAM_width(RAM_width),
         RAM_size(RAM_size), RAM_time(0) {
    memory.resize(RAM_size, MemoryCell());
}

void RAM::read(unsigned size) {
    RAM_time += RAM_read_time * (RAM_width / size + 1);
}

void RAM::write(unsigned size) {
    RAM_time += RAM_write_time * (RAM_width / size + 1);
}

void RAM::write_direct(addr_t addr, unsigned size, mem_t *value) {
    for (unsigned i = 0; i < size; ++i) {
        memory[addr + i] = MemoryCell(value[i]);
    }
}

unsigned RAM::size() {
    return memory.size();
}

void RAM::print(unsigned block_size) {
    for (unsigned i = 0; i < RAM_size; ++i) {
        if (i % block_size == 0) {
            std::cout << "\n";
            printf("%012X", i / block_size);
            std::cout << ": ";
        }
        if (memory[i].init) {
            printf("%02X", memory[i].get());
        }
        else {
            std::cout << "..";
        }
        std::cout << " ";
    }
}

Memory::Memory(Config &config) : reads(0), writes(0), read_misses(0), write_misses(0), back_writes(0),
               ram((*config.find("memory", "read_time")).get_int(),
                   (*config.find("memory", "write_time")).get_int(),
                   (*config.find("memory", "width")).get_int(),
                   (*config.find("memory", "size")).get_int()
                  ),
               cache((*config.find("cache", "read_time")).get_int(),
                     (*config.find("cache", "write_time")).get_int(),
                     (*config.find("cache", "size")).get_int(),
                     (*config.find("cache", "block_size")).get_int()
                    ) {
    if (ram.size() % cache.block_size) {
        std::cout << "\nblock_size must divide memory_size\n";
        exit(1);
    }
    unsigned associativity(0);
    unsigned cache_size = (*config.find("cache", "size")).get_int();
    unsigned block_size = (*config.find("cache", "block_size")).get_int();
    std::string str = (*config.find("cache", "associativity")).value;
    if (str == "direct") {
        associativity = cache_size / block_size;
    }
    if (str == "full") {
        associativity = 1;
    }
    if (!associativity) {
        associativity = cache_size / block_size / (*config.find("cache", "associativity")).get_int();
    }
    std::vector<std::pair<std::string, Algorithm>> algs = {
                                                           {"random", Algorithm::Random}, 
                                                           {"LRU", Algorithm::LRU}, 
                                                           {"LFU", Algorithm::LFU},
                                                           {"write-through", Algorithm::Write_through},
                                                           {"write-back", Algorithm::Write_back}
                                                          };
    Algorithm replacement_alg = Algorithm::Basic, write_alg = Algorithm::Basic;
    str = (*config.find("cache", "replacement_strategy")).value;
    for (auto &alg: algs) {
        if (str == alg.first) {
            replacement_alg = alg.second;        
        }
    }
    str = (*config.find("cache", "write_strategy")).value;
    for (auto &alg: algs) {
        if (str == alg.first) {
            write_alg = alg.second;        
        }
    }
    if (replacement_alg == Algorithm::Basic) {
        std::cout << "\nset replacement_strategy\n";
        exit(1);
    }
    if (write_alg == Algorithm::Basic) {
        std::cout << "\nset write_strategy\n";
        exit(1);
    }
    cache.set_params(config, associativity, replacement_alg, write_alg);
}

CacheIterator Memory::replace(addr_t addr) {
    auto iter = cache.find_replacement(addr);
    if (cache.back_to_RAM(iter, ram.memory)) {
         ++back_writes;
         ram.write(cache.block_size);
    }
    cache.change_structure_new(iter);
    ram.read(cache.block_size);
    (*iter).load_from_RAM(&ram.memory[addr], addr);
    cache.write();            //write from RAM                    ?????????????????
    return iter;
}

void Memory::correct_addr(addr_t addr, unsigned size) {
    if (addr >= ram.size()) {
        std::cout << "\nIncorrect trace\nAddress too long\n";
        exit(1);
    }
    if (size + addr % cache.block_size > cache.block_size) {
        std::cout << "\nIncorrect trace\nSize too long\n";
        exit(1);
    }
}

void Memory::read(addr_t addr, unsigned size, mem_t* value) {
    correct_addr(addr, size);
    if (cache.find_addr(addr) != cache.end(addr)) {
        ++reads;
        cache.read();
        auto iter = cache.find_addr(addr);
        cache.change_structure_old(iter);
    }
    else {
        ++read_misses;
        replace(addr);
        read(addr, size, value);
    }
}

void Memory::write(addr_t addr, unsigned size, mem_t* value) {
    correct_addr(addr, size);
    if (cache.find_addr(addr) != cache.end(addr)) {
        ++writes;
        auto iter = cache.find_addr(addr);
        cache.change_structure_old(iter);
        cache.write();
        if (!cache.write_direct(iter, addr, size, value)) {    //true if dirty used
            ram.write(size);
            ram.write_direct(addr, size, value);
        }
    }
    else {
        ++write_misses;
        if (cache.hit_write_direct()) {
            ++writes;
            ram.write(size);
            ram.write_direct(addr, size, value);
        }
        else {            
            replace(addr);
            write(addr, size, value);
        }
    }
}

void Memory::print() {
    ram.print(cache.block_size);
    cache.print();
    std::cout << "reads: " << reads << "\n";
    std::cout << "writes: " << writes << "\n";
    std::cout << "read_misses: " << read_misses << "\n";
    std::cout << "write_misses: " << write_misses << "\n";
    std::cout << "read_hits: " << reads - read_misses << "\n";
    std::cout << "write_hits: " << writes - write_misses << "\n";
    std::cout << "back_writes: " << back_writes << "\n";
    std::cout << "clocks: " << ram.RAM_time + cache.cache_time << "\n";
}














