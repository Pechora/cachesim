#include "algorithms.h"
#include "basic_memory_classes.h"
#include "parse_config.h"

#ifndef CACHE_CLASS_H
#define CACHE_CLASS_H

class Cache {
  private:
    unsigned associativity;
    unsigned cache_read_time;
    unsigned cache_write_time;

  public:
    unsigned cache_size;
    unsigned block_size;
    unsigned cache_time;

    ReplacementStrategy *replacement;
    WriteStrategy *writes;

    std::vector<CacheBlock> blocks;
    
    Cache() {}
    
    Cache(unsigned cache_read_time, unsigned cache_write_time,
          unsigned cache_size, unsigned block_size);

    void set_params(Config &config, unsigned associativity, Algorithm replacement_strategy, Algorithm write_strategy);

    ~Cache();

    CacheIterator find_replacement(addr_t addr);

    CacheIterator find_addr(addr_t addr);

    CacheIterator begin(addr_t addr);

    CacheIterator end(addr_t addr);

    void read();

    void write();

    bool back_to_RAM(CacheIterator iter, std::vector<MemoryCell> &memory);    //true, если dirty == true

    bool write_direct(CacheIterator iter, addr_t addr, unsigned size, mem_t* value);

    bool hit_write_direct();

    void change_structure_new(CacheIterator iter);

    void change_structure_old(CacheIterator iter);

    void print();

};

#endif //CACHE_CLASS_H

