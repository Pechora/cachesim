#include <vector>
#include <string>

#ifndef BASIC_MEMORY_CLASSES_H
#define BASIC_MEMORY_CLASSES_H

typedef unsigned addr_t;
typedef unsigned char mem_t;

struct MemoryCell {
  private:
    mem_t value;

  public:
    bool init;

    MemoryCell();

    mem_t get();

    explicit MemoryCell(mem_t value);
};

class CacheBlock {
  private:

  public:
    unsigned id;
    addr_t tag;
    bool init;
    std::vector<MemoryCell> cells;

    CacheBlock(unsigned block_size);

    void load_from_RAM(MemoryCell *cell, unsigned addr);

    void write_to_RAM(std::vector<MemoryCell> &memory);

    void load_direct(unsigned addr, unsigned size, mem_t* value);

    void set_id(unsigned new_id);
};

class CacheIterator {    //define associativity
  private:
    CacheBlock *begin;
    addr_t index;
    unsigned block_count;
    unsigned associativity;

  public:
    CacheIterator(CacheBlock *begin, unsigned index, unsigned block_count, unsigned associativity);

    CacheIterator& operator ++();

    CacheBlock& operator *();

    bool operator ==(CacheIterator it);

    bool operator !=(CacheIterator it);

    CacheIterator to_begin();

    CacheIterator to_end();
};

#endif //BASIC_MEMORY_CLASSES_H 



