#include <string>
#include <istream>

#ifndef TRACE_H
#define TRACE_H

class Trace {
  protected:
    std::string cut_comment(std::string);

    virtual bool fill_trace(std::istream &input);

    unsigned line;

  public:
    Trace();

    virtual ~Trace();

    enum Command {READ, WRITE};
    unsigned addr, size;
    long long value;
    Command com;

    bool get_trace(std::istream &input);
};

class Trace_full : public Trace {
  private:
    bool fill_trace(std::istream &input);
};

#endif //TRACE_H

