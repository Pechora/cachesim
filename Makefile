CXXFLAGS = -std=c++11 -O2 -Wall
CXX = g++
CXXFILES = $(wildcard *.cpp)
HFILES = $(wildcard *.h)
OBJECTS = $(CXXFILES:.cpp=.o)

all: cachesim

cachesim: $(OBJECTS)
	$(CXX) $(CXXFLAGS) -o cachesim $(OBJECTS)

include deps.make
deps.make: $(CXXFILES) $(HFILES)
	$(CXX) -MM $(CXXFILES) > deps.make

clean:
	rm -f *.o cachesim

