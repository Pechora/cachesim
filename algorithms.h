#include "basic_memory_classes.h"
#include <vector>

#ifndef ALGORITHMS_H
#define ALGORITHMS_H

enum class Algorithm {Basic, Random, LRU, LFU, Write_through, Write_back};

unsigned random(unsigned n);

class ReplacementStrategy {    //наследуются LRU, LFU, Random
  private:
    unsigned block_count;

  public:
    ReplacementStrategy(unsigned block_count);

    virtual ~ReplacementStrategy();

    virtual CacheIterator find(CacheIterator begin, CacheIterator end);

    virtual void change_structure_new(CacheIterator iter);

    virtual void change_structure_old(CacheIterator iter);
};

class RandomStrategy : public ReplacementStrategy { 
  public:
    RandomStrategy(unsigned block_count, unsigned seed);

    virtual CacheIterator find(CacheIterator begin, CacheIterator end);
};

class LRU_Strategy : public ReplacementStrategy {  
  private:
    std::vector<unsigned> queue;

    void change_structure(CacheIterator iter);

  public:
    LRU_Strategy(unsigned block_count);

    virtual CacheIterator find(CacheIterator begin, CacheIterator end);

    virtual void change_structure_new(CacheIterator iter);

    virtual void change_structure_old(CacheIterator iter);
};

class LFU_Strategy : public ReplacementStrategy {  
  private:
    unsigned max_value;
    unsigned init_value;
    unsigned become_old;
    unsigned make_young;
    std::vector<unsigned> age;
    std::vector<unsigned> cur_old;

  public:
    LFU_Strategy(unsigned block_count, unsigned max_value, unsigned init_value, unsigned become_old,
                 unsigned make_young, unsigned seed);

    virtual CacheIterator find(CacheIterator begin, CacheIterator end);

    virtual void change_structure_new(CacheIterator iter);

    virtual void change_structure_old(CacheIterator iter);
};

class WriteStrategy {
  private:
    unsigned block_count;

  public:
    bool hit_write_direct;

    WriteStrategy(unsigned block_count);

    virtual ~WriteStrategy();

    virtual bool dirty(addr_t id);

    virtual void set(addr_t id, bool flag);
};

class WriteBackStrategy : public WriteStrategy {
  private:
    std::vector<bool> dirty_blocks;

  public:
    //bool hit_write_direct;

    WriteBackStrategy(unsigned block_count);

    bool dirty(addr_t id);

    void set(addr_t id, bool flag);
};


#endif //ALGORITHMS_H



