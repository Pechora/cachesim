#include <map>
#include <string>
#include <iterator>

#ifndef PARSE_CONFIG_H
#define PARSE_CONFIG_H

class Config {
  private:
    std::map<std::string, std::map<std::string, std::string> > config;

    std::string cut_spaces(std::string);
    
  public:
    Config();

    explicit Config(std::string);
    
    struct Item {
        std::string section, key, value;

        Item(std::string section, std::string key, std::string value);
        
        long long get_int();
    };

    class Config_iterator : public std::iterator<std::input_iterator_tag, Item> {
        friend class Config;
      private:
        std::map<std::string, std::map<std::string, std::string> >::const_iterator section_it;
        std::map<std::string, std::string>::const_iterator key_value_it;
        std::map<std::string, std::map<std::string, std::string> >::const_iterator section_end;

        Config_iterator(std::map<std::string, std::map<std::string, std::string> >::const_iterator section_it,
                        std::map<std::string, std::map<std::string, std::string> >::const_iterator section_end);

        Config_iterator(std::map<std::string, std::map<std::string, std::string> >::const_iterator section_it,
                        std::map<std::string, std::string>::const_iterator key_value_it,
                        std::map<std::string, std::map<std::string, std::string> >::const_iterator section_end);
        
      public:
        Config_iterator& operator ++();

        Config_iterator operator ++(int);

        Item operator * ();

        bool operator == (Config_iterator it);

        bool operator != (Config_iterator it);
    };

    Config_iterator begin();

    Config_iterator end();

    Config_iterator find(std::string section, std::string key);

    typedef Config_iterator const_iterator;
};

#endif //PARSE_CONFIG_H


